import React from "react";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      globalTitle: ""
    }
  }

  componentDidMount() {
    console.log("Rendered Form");
  }

  componentDidUpdate() {
    console.log("Title changed");
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ globalTitle: "My form -" });
  }

  render() {
    return (
      <div className="App">
        <h1>live</h1>
        <form onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.title} onChange={(e) => this.setState({ title: e.target.value })}/>
          <button>Submit</button>
        </form>
      </div>
    );

  }
}

export default App;
